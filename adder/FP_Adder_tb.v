module FP_Adder_tb();

  reg  [31:0] tb_op1;
  reg  [31:0] tb_op2;
  wire [31:0] tb_sum;

  initial begin
    tb_op1 = 0;
    tb_op2 = 9;
  end

  FP_Adder fadder (tb_op1, tb_op2, tb_sum);

endmodule
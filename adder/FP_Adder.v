module FP_Adder
  (
    input  [31:0] op1,
    input  [31:0] op2,
    output reg [31:0] sum
  );

  always @(op1 or op2) begin
    sum = op1 + op2;
  end
  
endmodule
module FP_Mult
  (
    input  [31:0] op1,
    input  [31:0] op2,
    output reg [31:0] product
  );

  always @(op1 or op2) begin
    product = op1 + op2;
  end
  
endmodule
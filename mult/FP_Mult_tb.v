module FP_Mult_tb();

  reg  [31:0] tb_op1;
  reg  [31:0] tb_op2;
  wire [31:0] tb_prod;

  initial begin
    tb_op1 = 0;
    tb_op2 = 9;
  end

  FP_Mult fp_multiplier (tb_op1, tb_op2, tb_prod);

endmodule
iverilog -o .\out\adder.out .\adder\FP_Adder_tb.v .\adder\FP_Adder.v
vvp .\out\adder.out | Out-File -FilePath .\out\adder.output.txt

iverilog -o .\out\mult.out .\mult\FP_Mult_tb.v .\mult\FP_Mult.v
vvp .\out\mult.out | Out-File -FilePath .\out\mult.output.txt
